{{--
@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('A fresh verification link has been sent to your email address.') }}
                    </div>
                    @endif {{ __('Before proceeding, please check your email for a verification link.') }} {{ __('If you did not receive the
                    email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 --}}
@extends('plantillaInicio.welcome')
@section('carousel')
@endsection

@section('content')
<div class="container" style="margin-top:5%;">
    <div class=" row ">
        <div class="col-md-2 "> </div>
        <div class="col-md-8 ">
            <div class="card ">
                <div class="card-header bg-info ">
                    <h6>Confirmación de correo electrónico</h6>
                </div>
                <div class="card-body ">
                    @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('Mensaje de verificación enviado nuevamente...') }}
                    </div>
                    @endif
                    <h6>
                        <p align="justify ">Se ha enviado un mensaje de verificación a tu correo electrónico que nos has proporcionado, en caso
                            de no recibir el correo de confirmación, <a href="{{ route('verification.resend') }} ">presiona aquí</a>                            para volver a enviarlo.</p>
                    </h6>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function validarPass(){
        let pass1=$('#pass1').val();
        let pass2=$('#pass2').val();

        if(pass1!=pass2){
            $('#msj').css({'display':'inline'});
        return false;
        }else{
            $('#msj').css({'display':'none'});
            return true;
        }
    }

</script>
@endsection

@section('footer')
<div class="fixed-bottom">
    @include('plantillaInicio.footer')
</div>
@endsection
