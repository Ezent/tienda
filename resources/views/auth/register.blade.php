@extends('plantillaInicio.welcome')
@section('carousel')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3"> </div>
        <div class="col-md-6">
            <div class="card  ">
                <div class="card-header bg-info">
                    <h6>Crea tu cuenta</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('register') }}" onsubmit="return validarPass();" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Nombre</label>
                            <input id="name " type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"
                                required autofocus> @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span> @endif
                        </div>
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input type="text" value="{{ old( 'last_name') }}" required name="last_name" class="form-control" placeholder="Apellidos">                            @if($errors->has('last_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span> @endif
                        </div>
                        <div class="form-group ">
                            <label>Correo </label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"
                                required> @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span> @endif
                        </div>
                        <div class="form-group ">
                            <label>Contraseña </label>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                                required placeholder="**********"> @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span> @endif
                        </div>
                        <div class="form-group ">
                            <label>Confirmar contraseña </label>
                            <input required onblur="validarPass() " id="password2" type="password" name="password2" class="form-control" placeholder="**********">
                        </div>
                        <div id="msj" class="text-danger" style="display:none;">La contraseña no coíncide</div>
                        <br>
                        <font size=1> Usted esta consiente que acepta nuestros términos de licencia y es completamente mayor de edad para
                            recibir cualquier beneficio asi como para enfrentar aclaraciones fiscales.</font>
                        <button class="btn btn-block btn-info ">Aceptar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function validarPass(){
        let pass1=$('#password').val();
        let pass2=$('#password2').val();

        if(pass1!=pass2){
            $('#msj').css({'display':'inline'});
        return false;
        }else{
            $('#msj').css({'display':'none'});
            return true;
        }
    }

</script>
@endsection

@section('footer')
    @include('plantillaInicio.footer')
@endsection
