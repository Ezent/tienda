@extends('template_menu.template')
@section('link_default_template') @parent
<style>
    .fileRadius {
        border-radius: 5px;
    }
</style>
@endsection

@section('link_css')
@endsection

@section('content')
<div class="right_col" role="main">
    <!-- your content -->
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Configuraciones <small>.- Categorías</small></h3>
            </div>
        </div <div class="clearfix"></div>
    <!-- Categoria -->
    <div class="row">
        <div class="col-md-12 ">
            <div class="x_panel">
                <div class="x_title">
                    <h2>CATEGORÍAS <small>Configuracion</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                          <button onclick="editaCategoria('Hello word','add','categoria')" class="btn btn-xs"
                          type="button" data-toggle="modal" data-target="#myModal" style="background: #CFEC9E">
                                            Nuevo
                            </button>
                                @if ($categoria->isEmpty())
                                <div class="alert alert-danger">
                                    <h3></h3>No hay datos!!!</div>
                                @else
                                <table style="text-align: center;" id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap"
                                    cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">Categoria </th>
                                            <th style="text-align: center;">Registro</th>
                                            <th style="text-align: center;">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($categoria as $item)
                                        <tr>
                                            <td>{{ $item->categoria }}</td>
                                            <td>{{ $item->registro }}</td>
                                            <td>
                                                <button onclick="editaCategoria('{{ $item->categoria }}','edit','categoria')" class="btn btn-xs" type="button" data-toggle="modal" data-target="#myModal" style="background: #DADBBB">
                                                                Editar
                                                </button>
                                                <a class="btn btn-xs" onclick="deleteCate('{{ $item->id }}')" title="Eliminar Publicación"  style="background: #DADBBB;">
                                                                <i class="fa fa-close"></i>
                                                            </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Subcategoria-->
    <div class="row">
            <div class="col-md-12 ">
            <div class="x_panel">
                <div class="x_title">
                    <h2>SUBCATEGORIAS <small>Configuracion</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                @if ($subcategoria->isEmpty())
                                <div class="alert alert-danger">
                                    <h3></h3>No hay datos!!!</div>
                                @else
                                <table style="text-align: center;" id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap"
                                    cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">Subcategoria </th>
                                            <th style="text-align: center;">Registro</th>
                                            <th style="text-align: center;">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <a class="btn btn-default btn-xs">
                                                                Ver
                                                            </a>
                                                <a class="btn btn-xs" style="background: #DADBBB">
                                                                    Editar
                                                            </a>
                                                <a class="btn btn-danger btn-xs" title="Eliminar Publicación">
                                                                    <i class="fa fa-close"></i>
                                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Producto -->
    <div class="row">
            <div class="col-md-12 ">
            <div class="x_panel">
                <div class="x_title">
                    <h2>PRODUCTOS <small>Configuracion</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                @if($producto->isEmpty())
                                <div class="alert alert-danger">
                                    <h3></h3>No hay datos!!!</div>
                                @else
                                <table style="text-align: center;" id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap"
                                    cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">Producto</th>
                                            <th style="text-align: center;">Registro</th>
                                            <th style="text-align: center;">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <a class="btn btn-default btn-xs">
                                                    Ver
                                                </a>
                                                <a class="btn btn-xs" style="background: #DADBBB">
                                                        Editar
                                                </a>
                                                <a class="btn btn-danger btn-xs" title="Eliminar Publicación">
                                                        <i class="fa fa-close"></i>
                                                    </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form action="#">
            @csrf
            <div>
                    Categoria:
                    <input id="cat" type="text" name="categoria" class="form form-control"/>
            </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</div>
<div style="background: red">
<form action="{{  route('delete-categorias')  }}"  method="post">
@csrf
<input name="uno"  value="test" />
<input type="submit" value="Enviar" />
</div>
</form>
@endsection

@section('js_default_template') @parent
<script>
    function previewImg(evento, idPreview){
        imgPreview=document.getElementById(idPreview);
        imgPreview.src=URL.createObjectURL(evento.target.files[0]);
    }
    function deleteFoto(alias_id,foto){
        var url="{{route('ventas_publicaciones_delF')}}";
        var token=document.getElementsByName('_token')[0].value;
        var data={_token:token,id:alias_id,img:foto};
        $.post(url,data,function(msj){
            if(msj=='Eliminado correctamente'){
                location.reload(true);
            }else{
                alert(msj);
            }
        });
}

    function validate_fields(){
    var precio=document.getElementsByName('precio')[0].value;
    if( /^[0-9]+([.][0-9]{1,2})?$/.test(precio)==false){
        $('#alert_precio').html('No es un valor permitido');
    }else{
        $('#alert_precio').html('');
    }
}

/*img´s description plus or minus .- dinamic*/
function plusImg(){
    let form_group=document.getElementById('clonImg');
    let write_area=document.getElementById('imgs');
    let node_cloned=form_group.cloneNode(true);
    node_cloned.className="size-tags";
    //get Img preview cloned
    let preview=node_cloned.childNodes[1].childNodes[1];
    preview.src="{{asset('img/question.jpg')}}";
    let btn=node_cloned.childNodes[5].childNodes[1];
    let input=node_cloned.childNodes[3].childNodes[3];
    input.value="";
    input.required="true";
    btn.style.background='#E0F3D7';
    btn.style.color='green';
    let icono=node_cloned.childNodes[5].childNodes[1].childNodes[0];
    icono.className="fa fa-upload";
    numID_preview=document.querySelectorAll('.size-tags');
    preview.id="preview_"+(numID_preview.length+1);
    input.id=(numID_preview.length+1);
    input.name="img_desc_new[]";
    write_area.appendChild(node_cloned);
}
/*minus img*/
function minusImg(){
    var tags=document.querySelectorAll('.size-tags');
    if(tags.length>0){
        delete_node=tags[(tags.length-1)];
        delete_node.parentNode.removeChild(delete_node);
    }
}

/*ajax*/
/* Desvanecer advertencias*/
$(document).ready(function() {
    setTimeout(function() {
        $("#msj").fadeOut(1500);
    },4000);
});
/*visualizar las img*/
    $("#img_p").change(function () {
        filePreview(this);
        });
        function filePreview(input) {
        if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#img_principal').remove();
        $('#content_img_p + embed').remove();
        $('#content_img_p').after('<embed class="img-thumbnail col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3 " src="' + e.target.result + '" width="60" height="50">');
        }
        reader.readAsDataURL(input.files[0]);
        }
        }
        //categorias
function editaCategoria(value,action,section){
    if(section=='categoria'){
        let categoria=document.getElementById('cat');
        if(action=='add'){
            categoria.value='';
        }else{
            categoria.value=value;
        }
    }
}
function deleteCate(id){
    var url="{{ route('delete-categorias') }}";
    var token=document.getElementsByName('_token')[0].value;
    var data={_token:token, id_cat:id};

            $.post(url,data, function(msj){
                alert(msj);
            });

}
</script>

@endsection
