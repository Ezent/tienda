@extends('template_menu.template') 
@section('link_default_template') @parent
<style>
    .fileRadius {
        border-radius: 5px;
    }
</style>
@endsection
 
@section('link_css')
@endsection
 
@section('content')
<div class="right_col" role="main">
    <!-- your content -->
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Publicaciones <small>.- Editar publicación</small></h3>
            </div>
        </div>
        <div class="pull-right">
            <a href="{{route('ventas_publicaciones')}}" class="btn btn-default btn-xs">Regresar</a>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="x_panel">
                    @if (session('msj'))
                    <div id="msj" class="alert alert-success has-success has-feedback">
                        {{session('msj')}}
                    </div>
                    @endif
                    <!-- validar error de imagen-->
                    @if ($errors->any())
                    <div id="msj" class="alert alert-danger has-success has-feedback">
                        @foreach ($errors->all() as $item)
                        <i>{{$item}}</i><br> @endforeach
                    </div>
                    @endif

                    <div class="x_title">
                        <h2>Publicaciones<small></small></h2>
                        <h6 class="pull-right">Requeridos <i class="text-danger">*</i></h6>
                        <div class="x_panel">
                            <div class="x_content">
                                <br />
                                <form id="pub_form" class="form-horizontal form-label-left" action="{{route('ventas_publicacionesEdit')}}" method="post"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <!-- id de la publicaciones oculto-->
                                    <input type="hidden" name="id_publicaciones" value="{{$edit->id}}">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre Publicación <i class="text-danger">*</i>
                                        </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input name="nombrePublicacion" type="text" required value="{{old('nombrePublicacion',$edit->nombrePublicacion)}}" class="form-control col-md-7 col-xs-12"
                                                    placeholder="Ej. Llave para filtro de vehiculos">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"> Precio  <i class="text-danger">*</i>
                                         </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input onkeyup="validate_fields()" type="text" name="precio" value="{{old('precio',$edit->precio)}}" required class="form-control col-md-7 col-xs-12"
                                                    placeholder="solo números positivos sin espacios">
                                                <div id="alert_precio" class="alert alert-danger text-danger"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Categoría  <i class="text-danger">*</i></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select onchange="loadSelect(1)" id="categoria" name="categoria" required class="form-control col-md-7 col-xs-12">
                                                <option label="Selecciona una opcion ..."></option>
                                                @foreach ($categoria as $item)
                                               <option value="{{$item->id}}" {{($item->id==old('categoria',$edit->categoria_id)) ? 'selected' : ''}}>
                                                {{$item->categoria}}  
                                            </option>
                                               @endforeach
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group" id="tag_subCat">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Sub-categoría  <i class="text-danger">*</i></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select onchange="loadSelect(2)" id="subCat" name="subcategoria" required class="form-control col-md-7 col-xs-12">
                                                <option label="Selecciona una opcion ..."></option>
                                                @foreach ($subcategoria as $item)
                                                  <option value="{{$item->id}}" {{($item->id==old('subcategoria',$edit->subcategoria_id)) ? 'selected' : ''}}>{{$item->subcategoria}}</option>  
                                                @endforeach
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group" id="tag_producto">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tipo Producto  <i class="text-danger">*</i></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select id="producto" name="producto" required class="form-control col-md-7 col-xs-12">
                                                 <option label="Selecciona una opcion ..."></option>
                                                 @foreach ($producto as $item)
                                                 <option value="{{$item->id}}" {{($item->id==old('producto',$edit->producto_id)) ? 'selected' : ''}}>{{$item->producto}}</option>  
                                               @endforeach
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripción
                                         </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea required name="descripcion" class="date-picker form-control col-md-7 col-xs-12" placeholder="Escribe los detalles de tu publicación.">{{old('descripcion',$edit->descripcion)}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    {{--
                                    <div id="content_img_p" class="form-group  has-success has-feedback">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Imagen Principal <i class="text-danger">*</i>
                                          </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="img_p" name="img_principal" type="file" required class="form-control col-md-7 col-xs-12">
                                            <embed id="img_principal" width="60" height="50">
                                        </div>
                                    </div> --}}
                                    <!-- Input dinamico-->
                                    {{--
                                    <div class="col-md-12"></div>
                                    <div class="form-group has-warning has-feedback">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12 " for="first-name">Imagenes Descriptivas <i class="text-danger">*</i>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12" id="clon_img">
                                            <input name="img_desc[]" type="file" required class="form-control col-md-7 col-xs-12">
                                        </div>
                                        <div id="imgs"></div>
                                    </div> --}}
                                    <hr/>
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Imagen</th>
                                                        <th>Seleccionar</th>
                                                        <th>Eliminar</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="imgs">
                                                    <?php $cont=0; ?> @for ( $i=0; $i
                                                    < count($foto); $i++) @if ($foto[$i]->principal=='yes')
                                                        <tr colspan="3">
                                                            <td colspan="3" style="background: #FCFBFA">
                                                                <p align="center">Imagen Principal</p>
                                                            </td>
                                                        </tr>
                                                        @elseif($cont==0)
                                                        <?php $cont++; ?>
                                                        <tr colspan="3">
                                                            <td colspan="3" style="background: #FCFBFA">
                                                                <p align="center">Imagenes para describir la publicación</p>
                                                            </td>
                                                        </tr>
                                                        @endif
                                                        <tr id="clonImg" {{-- id="{{($i==(count($foto)-1) && count($foto) > 1 ) ? 'clonImg':''}}" --}}>
                                                            @if(!$foto[$i]->foto==null)
                                                            <td>
                                                                <img id="<?php if($foto[$i]->principal=='yes') {?>preview_0<?php }else{ ?>img_desc{{$foto[$i]->id}}<?php } ?>" style="width: 6rem"
                                                                    src="https://s3.us-east-2.amazonaws.com/tienda-online/{{$foto[$i]->foto}}"
                                                                    class="img-fluid" />
                                                            </td>
                                                            @endif
                                                            <td>
                                                                <input type="hidden" name="id_foto_update[]" value="{{$foto[$i]->id}}">
                                                                <input id="{{($foto[$i]->principal=='yes') ? 0 : $foto[$i]->id}}" onchange="previewImg(event,'{{($foto[$i]->principal=='yes') ? 'preview_' : 'img_desc'}}'+this.id)"
                                                                    class="fileRadius
                                                                form-control" name="{{($foto[$i]->principal=='yes')? 'img_p'
                                                                : 'img_desc['.$foto[$i]->id.']'}}" type="file" accept="image/*"
                                                                />
                                                                <!--input para pasar nombre de la img -->
                                                                <input type="hidden" name="{{($foto[$i]->principal=='yes') ? 'name_foto_p' : 'name_foto_desc['.$foto[$i]->id.']'}}" value="{{ $foto[$i]->foto}}">
                                                            </td>
                                                            <td>
                                                                @if ($foto[$i]->principal=='yes')
                                                                <button class="btn btn-default" disabled style="background: #E9B8AC;"><i class="fa fa-close"></i></button>                                                                @else
                                                                <button type="button" onclick="deleteFoto('{{$foto[$i]->id}}','{{$foto[$i]->foto}}')" class="btn btn-default"><i class="fa fa-close" title="Eliminar foto de la publicación"></i></button>                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @endfor @if ($foto->isEmpty()|| count($foto)
                                                        <2) @if (count($foto)==0 || $foto->isEmpty())
                                                            <tr colspan="3">
                                                                <td colspan="3" style="background: #FCFBFA">
                                                                    <p align="center">Imagen Principal</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <img id="preview" style="width: 4rem" src="{{asset('/img/user.png')}}" class="img-fluid" />
                                                                </td>
                                                                <td><input onchange="previewImg(event,'preview')" class="form-control fileRadius"
                                                                        type="file" name="img_p" /></td>
                                                                <td><button class="btn btn-default" disabled><i class="fa fa-close"></i></button></td>
                                                            </tr>
                                                            @endif @if (count($foto)==1 || $foto->isEmpty())
                                                            <tr>
                                                                <td colspan="3" style="background: #FCFBFA">
                                                                    <p align="center">Imagenes para describir la publicación</p>
                                                                </td>
                                                            </tr>
                                                            @endif @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <hr/>
                                    <label class="pull pull-right">Añadir nuevas imágnes</label>
                                    <div class="col-md-offset-3 col-md-6 ">
                                        <button onclick="plusImg()" type="button" class="btn btn-succes btn-xs pull-right"><i class="fa fa-plus"></i></button>
                                        <button onclick="minusImg()" type="button" class="btn btn-succes btn-xs pull-right"><i class="fa fa-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <button class="btn btn-primary" type="reset">Cancelar</button>
                                            <button type="submit" class="btn btn-success">Enviar</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 
@section('js_default_template') @parent
<script>
    function previewImg(evento, idPreview){
        imgPreview=document.getElementById(idPreview);
        imgPreview.src=URL.createObjectURL(evento.target.files[0]);
    }
    function deleteFoto(alias_id,foto){
        var url="{{route('ventas_publicaciones_delF')}}";
        var token=document.getElementsByName('_token')[0].value;
        var data={_token:token,id:alias_id,img:foto};
        $.post(url,data,function(msj){
            if(msj=='Eliminado correctamente'){
                location.reload(true);
            }else{
                alert(msj);
            }
        });
}

    function validate_fields(){
    var precio=document.getElementsByName('precio')[0].value;
    if( /^[0-9]+([.][0-9]{1,2})?$/.test(precio)==false){
        $('#alert_precio').html('No es un valor permitido');
    }else{
        $('#alert_precio').html('');
    }
}

/*img´s description plus or minus .- dinamic*/
function plusImg(){
    let form_group=document.getElementById('clonImg');
    let write_area=document.getElementById('imgs');
    let node_cloned=form_group.cloneNode(true);
    node_cloned.className="size-tags";
    //get Img preview cloned
    let preview=node_cloned.childNodes[1].childNodes[1];
    preview.src="{{asset('img/question.jpg')}}";
    let btn=node_cloned.childNodes[5].childNodes[1];
    let input=node_cloned.childNodes[3].childNodes[3];
    input.value="";
    input.required="true";
    btn.style.background='#E0F3D7';
    btn.style.color='green';
    let icono=node_cloned.childNodes[5].childNodes[1].childNodes[0];
    icono.className="fa fa-upload";
    numID_preview=document.querySelectorAll('.size-tags');
    preview.id="preview_"+(numID_preview.length+1);
    input.id=(numID_preview.length+1);
    input.name="img_desc_new[]";
    write_area.appendChild(node_cloned);
}
/*minus img*/
function minusImg(){
    var tags=document.querySelectorAll('.size-tags');
    if(tags.length>0){
        delete_node=tags[(tags.length-1)];
        delete_node.parentNode.removeChild(delete_node);
    }
}

/*ajax*/
function loadSelect(variable){
    /*1 para el calculo de subcategorias*/
    /*2 para el calculo de Productos*/
  var id_=(variable==1) ? $('#categoria').val() : $('#subCat').val();
 var token=document.getElementsByName('_token')[0].value;
    var url="{{route('ventas_publicaciones_form')}}"+"/"+variable;
  var data={_token:token, id: id_};

    $.post(url,data,function(msj){
        var resp=msj.map(function(obj){
        return "<option value='"+obj.id+"'>"+(obj.subcategoria || obj.producto)+"</option>";
        });
        var relleno='<option label="Selecciona una opcion ..."></option>';
        if(variable==1){
            if(resp.length>0){
                document.getElementById('tag_subCat').style.display="block";
                document.getElementById('subCat').innerHTML=relleno;
                document.getElementById('subCat').innerHTML+=resp;
            }else{
                document.getElementById('tag_subCat').style.display="none";
                document.getElementById('tag_producto').style.display="none";
            }
        }
        else if(variable==2){
            if(resp.length>0){
                document.getElementById('tag_producto').style.display="block";
                document.getElementById('producto').innerHTML=relleno;
                document.getElementById('producto').innerHTML+=resp;
            }else{
                document.getElementById('tag_producto').style.display="none";
            }
            
        }
    });
}
/* Desvanecer advertencias*/
$(document).ready(function() {
    setTimeout(function() {
        $("#msj").fadeOut(1500);
    },4000);
});
/*visualizar las img*/
    $("#img_p").change(function () {
        filePreview(this);
        });
        function filePreview(input) {
        if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#img_principal').remove();
        $('#content_img_p + embed').remove();
        $('#content_img_p').after('<embed class="img-thumbnail col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3 " src="' + e.target.result + '" width="60" height="50">');
        }
        reader.readAsDataURL(input.files[0]);
        }
        }

</script>
@endsection