@extends('template_menu.template') 
@section('link_default_template') @parent
<link href="{{asset('p_menu/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('p_menu/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('p_menu/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('p_menu/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('p_menu/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">
@endsection
 
@section('content')
<!-- todo el contenido de las publicaciones -->
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Publicaciones <small>.- Todas tus publicaciones en venta</small></h3>
            </div>
        </div>
        <div class="pull-right">
            <a href="{{route('ventas_publicaciones_form')}}" class="btn btn-info btn-xs">Añadir nueva</a>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Publicaciones<small></small></h2>
                        {{--
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul> --}}
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        {{--
                        <p class="text-muted font-13 m-b-30">
                            Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen
                            sizes through the dynamic insertion and removal of columns from the table.
                        </p> --}} @if (!$data==null)
                        <table style="text-align: center;" id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap"
                            cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">Foto </th>
                                    <th style="text-align: center;">Publicación </th>
                                    <th style="text-align: center;">Precio $</th>
                                    <th style="text-align: center;">Producto</th>
                                    <th style="text-align: center;">Estado</th>
                                    <th style="text-align: center;">Fecha</th>
                                    <th style="text-align: center;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                <tr>
                                    <td>
                                        @if (!$item->foto==null)
                                        <img class="img-fluid rounded" src="https://s3.us-east-2.amazonaws.com/tienda-online/{{$item->foto}}" width="60" height="40"
                                        /> @else
                                        <h2><i class="fa fa-ban fa-xl"></i></h2>Información incompleta @endif
                                    </td>
                                    <td>{{$item->publicacion}}</td>
                                    <td>{{$item->precio}}</td>
                                    <td>{{$item->producto}}</td>
                                    <td>
                                        @if ($item->estado=='ALTA')
                                        <p class="label label-success">{{$item->estado}}</p>
                                        @else
                                        <p class="label label-danger">{{$item->estado}}</p>
                                        @endif
                                    </td>
                                    <td>{{$item->fecha}}</td>
                                    <!-- Acciones para agregar las acciones del crud -->
                                    <td>
                                        <a onclick="show_publication('{{$item->id}}','ver')" class="btn btn-default btn-xs">
                                            Ver
                                        </a>
                                        <a class="btn btn-xs" style="background: #DADBBB" href="{{route('ventas_publicaciones_edit').'/'.$item->id}}">
                                                Editar
                                        </a> @if ($item->estado=='ALTA')
                                        <a onclick="publication('{{$item->id}}','baja');" class="btn btn-warning btn-xs" title="Baja temporal a la publicación">
                                                Baja
                                            </a> @else
                                        <a onclick="publication('{{$item->id}}','alta');" class="btn btn-success btn-xs" title="Activa la publicación">
                                                Alta
                                            </a> @endif
                                        <a onclick="deletePub('{{$item->id}}')" class="btn btn-danger btn-xs" title="Eliminar Publicación">
                                                <i class="fa fa-close"></i>
                                            </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        <div class="alert alert-info">
                            <h3>No tienes ninguna publicación</h3>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Modal boton activa -->
    <a id="activa" data-toggle="modal" data-target="#myModal"></a>
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 id="title_modal" class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <div role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Imágenes</a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Características</a>
                            </li>
                            <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Descripción</a>
                            </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div style="overflow: auto; height: 200px;" role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                            </div>
                            <div style="overflow: auto; height: 200px;" role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                            </div>
                            <div style="overflow: auto; height: 200px;" role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!--fin Modal-->
</div>
<!-- /page content -->
<!--token-->
@csrf
@endsection
 
@section('js_default_template') @parent
<script src="{{asset('p_menu/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('p_menu/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('p_menu/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('p_menu/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('p_menu/vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('p_menu/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('p_menu/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('p_menu/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
<script src="{{asset('p_menu/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('p_menu/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('p_menu/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
<script src="{{asset('p_menu/vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
<script src="{{asset('p_menu/vendors/jszip/dist/jszip.min.js')}}"></script>
<script src="{{asset('p_menu/vendors/pdfmake/build/pdfmake.min.js')}}"></script>
<script src="{{asset('p_menu/vendors/pdfmake/build/vfs_fonts.js')}}"></script>

<script type="text/javascript">
    function deletePub(id_pub){
        let token=document.getElementsByName('_token')[0].value;
        let url="{{route('ventas_publicaciones_delP')}}";
        let data={_token:token,id:id_pub};
        $.post(url,data, function(msj){
            if(msj=='Eliminada'){
            location.reload(true);
            }
        });
    }
    <!-- script para ver las publicaciones-->
    function show_publication(id_pub,action){ 
        var url="{{route('ventas_publicaciones')}}";
        var token=document.getElementsByName('_token')[0].value;
        var data={_token: token, accion:action, id:id_pub};
        var title=document.getElementById('title_modal');
        var carateristicas=document.getElementById('tab_content2');
        var description=document.getElementById('tab_content3');

        $.post(url,data,function(msj){
            if(msj.length > 0){
                var status=(msj[0].estado=='ALTA') ? 'text-success' : 'text-danger';
                var propeties='<div class="text text-center"><b>Tipo de Producto: </b>'
               +'<p>'+msj[0].producto+'</p>'
               +'<hr/>'
               +'<b>Precio</b>'
               +'<p>'+msj[0].precio+'</p>'
               +'<hr/>'
               +'<b>Status de la publicación</b>'
               +'<p class="'+status+'"><b>'+msj[0].estado+'</b></p>'
               +'<hr/>'
               +'<b>Fecha de lanzamiento de la publicación</b>'
               +'<p>'+msj[0].fecha+'</p></div>'
               ;
            title.innerHTML=msj[0].publicacion;
            carateristicas.innerHTML=propeties;
            description.innerHTML=msj[0].descripcion;
            /*ciclo inverso para q siempre aparesca primero la img principal*/
            var data="";
             for(var i=(msj.length-1); i>=0; i--){
                 var img='<img class="img-responsive" src="https://s3.us-east-2.amazonaws.com/tienda-online/'+msj[i].foto+'"><hr/>';
                data+=img;
             }
            document.getElementById('tab_content1').innerHTML=data;
            $('#activa').click();
            }else{alert('Información incompleta, por favor actualizala !!!');}
        });
}
function publication(id_pub,accion){
    let url="{{route('ventas_publicaciones_low_up')}}";
    let token=document.getElementsByName('_token')[0].value;
    let data={_token: token, id:id_pub,action:accion};
    $.post(url,data,function(msj){
        alert(msj);
        window.location.href="{{route('ventas_publicaciones')}}";
    });
}

</script>
@endsection