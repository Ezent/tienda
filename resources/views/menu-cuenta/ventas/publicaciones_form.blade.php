@extends('template_menu.template') 
@section('link_default_template') @parent
@endsection
 
@section('content')
<div class="right_col" role="main">
    <!-- your content -->
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Publicaciones <small>.- Agregar nueva publicación</small></h3>
            </div>
        </div>
        <div class="pull-right">
            <a href="{{route('ventas_publicaciones')}}" class="btn btn-default btn-xs">Regresar</a>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 ">
                <div class="x_panel">
                    @if (session('msj'))
                    <div id="msj" class="alert alert-success has-success has-feedback">
                        {{session('msj')}}
                    </div>
                    @endif
                    <!-- validar error de imagen-->
                    @if ($errors->any())
                    <div id="msj" class="alert alert-danger has-success has-feedback">
                        @foreach ($errors->all() as $item)
                        <i>{{$item}}</i><br> @endforeach
                    </div>
                    @endif

                    <div class="x_title">
                        <h2>Publicaciones<small></small></h2>
                        <h6 class="pull-right">Requeridos <i class="text-danger">*</i></h6>
                        <div class="x_panel">
                            <div class="x_content">
                                <br />
                                <form id="pub_form" class="form-horizontal form-label-left" action="{{route('ventas_publicaciones_form_add')}}" method="post"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre Publicación <i class="text-danger">*</i>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input name="nombrePublicacion" type="text" required value="{{old('nombrePublicacion')}}" class="form-control col-md-7 col-xs-12"
                                                placeholder="Ej. Llave para filtro de vehiculos">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"> Precio  <i class="text-danger">*</i>
                                         </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input onkeyup="validate_fields()" type="text" name="precio" value="{{old('precio')}}" required class="form-control col-md-7 col-xs-12"
                                                placeholder="solo números positivos sin espacios">
                                            <div id="alert_precio" class="alert alert-danger text-danger"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Categoría  <i class="text-danger">*</i></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select onchange="loadSelect(1)" id="categoria" name="categoria" required class="form-control col-md-7 col-xs-12">
                                                <option label="Selecciona una opcion ..."></option>
                                                @foreach ($categoria as $item)
                                               <option  value="{{$item->id}}" {{(old('categoria')==$item->id) ? 'selected' : ''}}>
                                                {{$item->categoria}}  
                                            </option>
                                               @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" style="display: none" id="tag_subCat">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Sub-categoría  <i class="text-danger">*</i></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select onchange="loadSelect(2)" id="subCat" name="subcategoria" required class="form-control col-md-7 col-xs-12">
                                                <option label="Selecciona una opcion ..."></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" style="display: none" id="tag_producto">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tipo Producto  <i class="text-danger">*</i></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="producto" name="producto" required class="form-control col-md-7 col-xs-12">
                                                <option label="Selecciona una opcion ..."></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripción
                                         </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea required name="descripcion" class="date-picker form-control col-md-7 col-xs-12" placeholder="Escribe los detalles de tu publicación.">{{old('descripcion')}}</textarea>
                                        </div>
                                    </div>
                                    <div id="content_img_p" class="form-group  has-success has-feedback">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Imagen Principal <i class="text-danger">*</i>
                                          </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="img_p" name="img_principal" type="file" required class="form-control col-md-7 col-xs-12">
                                            <embed id="img_principal" width="60" height="50">
                                        </div>
                                        <!--Aqui vamos a mostrar nuestra img -->
                                    </div>
                                    <!-- Input dinamico-->
                                    <div class="col-md-12"></div>

                                    <div class="form-group has-warning has-feedback">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12 " for="first-name">Imagenes Descriptivas <i class="text-danger">*</i>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12" id="clon_img">
                                            <input name="img_desc[]" type="file" required class="form-control col-md-7 col-xs-12">
                                        </div>
                                        <div id="imgs"></div>
                                    </div>
                                    <div class="col-md-offset-3 col-md-6 ">
                                        <button onclick="plusImg()" type="button" class="btn btn-succes btn-xs pull-right"><i class="fa fa-plus"></i></button>
                                        <button onclick="minusImg()" type="button" class="btn btn-succes btn-xs pull-right"><i class="fa fa-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <button class="btn btn-primary" type="reset">Cancelar</button>
                                            <button type="submit" class="btn btn-success">Enviar</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 
@section('js_default_template') @parent
<script>
    $(document).ready(function(){
        uno();
    });
    function uno(){
        var cat=document.getElementById('categoria').value;
        if(!cat==null || !cat==''){
        console.log('ha cargado');
        loadSelect(1)
        }else{
            console.log('ha cargado pero no el select');
        }
    }
    function validate_fields(){
    var precio=document.getElementsByName('precio')[0].value;
    if( /^[0-9]+([.][0-9]{1,2})?$/.test(precio)==false){
        $('#alert_precio').html('No es un valor permitido');
    }else{
        $('#alert_precio').html('');
    }
}

/*img´s description plus or minus .- dinamic*/
function plusImg(){
    let form_group=document.getElementById('clon_img');
    let write_area=document.getElementById('imgs');
    let node_cloned=form_group.cloneNode(true);
    node_cloned.className="col-md-offset-3 col-md-6 size-tags";
    node_cloned.childNodes[1].value=null;
    write_area.appendChild(node_cloned);
}
/*minus img*/
function minusImg(){
    var tags=document.querySelectorAll('.size-tags');
    if(tags.length>0){
        delete_node=tags[(tags.length-1)];
        delete_node.parentNode.removeChild(delete_node);
    }
}

/*ajax*/
function loadSelect(variable){
    /*1 para el calculo de subcategorias*/
    /*2 para el calculo de Productos*/
  var id_=(variable==1) ? $('#categoria').val() : $('#subCat').val();
 var token=document.getElementsByName('_token')[0].value;
    var url="{{route('ventas_publicaciones_form')}}"+"/"+variable;
  var data={_token:token, id: id_};

    $.post(url,data,function(msj){
        var resp=msj.map(function(obj){
        return "<option value='"+obj.id+"'>"+(obj.subcategoria || obj.producto)+"</option>";
        });
        var relleno='<option label="Selecciona una opcion ..."></option>';
        if(variable==1){
            if(resp.length>0){
                document.getElementById('tag_subCat').style.display="block";
                document.getElementById('subCat').innerHTML=relleno;
                document.getElementById('subCat').innerHTML+=resp;
            }else{
                document.getElementById('tag_subCat').style.display="none";
                document.getElementById('tag_producto').style.display="none";
            }
        }
        else if(variable==2){
            if(resp.length>0){
                document.getElementById('tag_producto').style.display="block";
                document.getElementById('producto').innerHTML=relleno;
                document.getElementById('producto').innerHTML+=resp;
            }else{
                document.getElementById('tag_producto').style.display="none";
            }
            
        }
    });
}
/* Desvanecer advertencias*/
$(document).ready(function() {
    setTimeout(function() {
        $("#msj").fadeOut(2000);
    },6000);
});
/*visualizar las img*/
    $("#img_p").change(function () {
        filePreview(this);
        });
        function filePreview(input) {
        if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
        $('#img_principal').remove();
        $('#content_img_p + embed').remove();
        $('#content_img_p').after('<embed class="img-thumbnail col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3 " src="' + e.target.result + '" width="60" height="50">');
        }
        reader.readAsDataURL(input.files[0]);
        }
        }

</script>
@endsection