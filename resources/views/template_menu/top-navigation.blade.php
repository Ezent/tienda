<div class="top_nav">
    <div class="nav_menu" style="background: #3681AF;">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars" style="color: #C0D7E6;"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <!--Inicio del menu del user -->
                @if(auth()->user())
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="images/img.jpg" alt="" />
                        <b>{{ auth()->user()->name }}</b>
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        {{--
                        <li>
                            <a href="javascript:;">
                                <span class="badge bg-red pull-right">50%</span>
                                <span>Settings</span>
                            </a>
                        </li> --}}
                        <li>
                            <a href="{{ url('/') }}"><i class="fa fa-bookmark pull-right"></i>Start</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-user pull-right"></i>Perfil</a>
                        </li>
                        <li>
                            <a href="{{ route('close_session') }}"><i class="fa fa-sign-out pull-right"></i> Salir</a>
                        </li>
                    </ul>
                </li>
                @endif
                <!--Fin del menu del users-->
                <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-bell"></i>
                        <span class="badge bg-green">6</span>
                    </a>
                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                        <li>
                            <a>
                                <span class="image"
                                      ><img src="images/img.jpg" alt="Profile Image"
                                      /></span>
                                <span>
                                    <span>John Smith</span> <span class="time">3 mins ago</span>
                                </span>
                                <span class="message">
                                    Film festivals used to be do-or-die moments for movie makers.
                                    They were where...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a>
                                <span class="image"
                                      ><img src="images/img.jpg" alt="Profile Image"
                                      /></span>
                                <span>
                                    <span>John Smith</span> <span class="time">3 mins ago</span>
                                </span>
                                <span class="message">
                                    Film festivals used to be do-or-die moments for movie makers.
                                    They were where...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a>
                                <span class="image"
                                      ><img src="images/img.jpg" alt="Profile Image"
                                      /></span>
                                <span>
                                    <span>John Smith</span> <span class="time">3 mins ago</span>
                                </span>
                                <span class="message">
                                    Film festivals used to be do-or-die moments for movie makers.
                                    They were where...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a>
                                <span class="image"
                                      ><img src="images/img.jpg" alt="Profile Image"
                                      /></span>
                                <span>
                                    <span>John Smith</span> <span class="time">3 mins ago</span>
                                </span>
                                <span class="message">
                                    Film festivals used to be do-or-die moments for movie makers.
                                    They were where...
                                </span>
                            </a>
                        </li>
                        <li>
                            <div class="text-center">
                                <a>
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <form class="navbar-collapse collapse navbar-form top_search" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                                  <button class="btn btn-default" type="button">Go!</button>
                                                </span>
                        </div>
                    </form>
                </li>
            </ul>
        </nav>
    </div>
</div>