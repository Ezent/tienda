<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{route('start')}}" class="site_title"><i class="fa fa-paw"></i> <span>Shop Montecristo</span></a>
        </div>
        <div class="clearfix"></div>
        <!-- menu profile quick info -->
        {{--
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="images/img.jpg" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>John Doe</h2>
            </div>
        </div> --}}
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-home"></i> Mi cuenta </a> {{--
                        <ul class="nav child_menu">
                            <li><a href="index.html"></a></li>
                            <li><a href="index2.html"></a></li>
                            <li><a href="index3.html"></a></li>
                        </ul> --}}
                    </li>
                    <li><a><i class="fa fa-table"></i> Ventas <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{route('ventas_publicaciones')}}">Publicaciones</a></li>
                            <li><a href="tables_dynamic.html">Preguntas</a></li>
                            <li><a href="tables_dynamic.html">Ventas</a></li>
                            <li><a href="tables_dynamic.html">Datos de los Interesados</a></li>
                            {{--
                            <li><a href="tables_dynamic.html">Publicidad</a></li> --}}
                        </ul>
                    </li>
                    <li><a><i class="fa fa-desktop"></i> Compras <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="general_elements.html">Favoritos</a></li>
                            <li><a href="media_gallery.html">Preguntas</a></li>
                            <li><a href="typography.html">Cotizaciones</a></li>
                            <li><a href="icons.html">Compras</a></li>
                            <li><a href="glyphicons.html">Suscripciones</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-edit"></i> Reputación <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="form.html">Configurar</a></li>
                            <li><a href="form_advanced.html">Respuestas negativas</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-cogs"></i>Configuración <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="chartjs.html">Cuenta</a></li>
                            <li><a href="chartjs2.html">Libreta de direcciones</a></li>
                            <li><a href="{{ route('view-catorias') }}">Categoras</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
      </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
      </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
