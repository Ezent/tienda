@extends('plantillaInicio.welcome')
@section('carousel')
@endsection

@section('content')
<div class="container" style="margin-top:5%;">
    <div class=" row ">
        <div class="col-md-2 "> </div>
        <div class="col-md-8 ">
            <div class="card ">
                <div class="card-header bg-info ">
                    <h6>Confirmación de correo electrónico</h6>
                </div>
                <div class="card-body">
                    @if(session('msj'))
                    <div class="alert alert-success">
                        <h6>
                            {{ session('msj') }}
                        </h6>
                    </div> @endif
                    <br>
                    <h6>
                        <p align="justify ">Se ha enviado un mensaje de verificación a tu correo electrónico que nos has proporcionado, en caso
                            de no recibir el correo de confirmación, <a href='{{ route("resendEmail-verificacion") }}'>presiona aquí</a>                            para volver a enviarlo.</p>
                    </h6>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function validarPass(){
        let pass1=$('#pass1').val();
        let pass2=$('#pass2').val();

        if(pass1!=pass2){
            $('#msj').css({'display':'inline'});
        return false;
        }else{
            $('#msj').css({'display':'none'});
            return true;
        }
    }

</script>
@endsection

@section('footer')
<div class="fixed-bottom">
    @include('plantillaInicio.footer')
</div>
@endsection
