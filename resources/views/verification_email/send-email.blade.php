<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shop Montecristo</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
</head>

<body>
    <!--Aqui va el contenido -->

    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="jumbotron">
                    <h1 class="display-4">Hola ! ...</h1>
                    <p class="lead">Shop Montecristo te da la vienvenida a nuestro equipo, como parte de nuestras politicas de seguridad,
                        necesitamos que nos confirmes tu correo eléctronico.
                    </p>
                    <hr class="my-4">
                    <p>Es importante que nos confirmes tu dirección de correo electronico ya que a tráves de él nos mantendremos
                        en contacto contigo solo necesitas dar clic en el boton de <b>Confirmar</b>.
                    </p>
                    <a class="btn btn-primary btn-lg" href='{{ url("/register/verify")."/".$confirmation_code }}' role="button">Confirmar</a>
                    <p>
                        <font size=1>En caso de no ser la persona que ha otorgado esta dirección de correo elctronico no realice nada
                            y nuestro sistema en un determinado tiempo se encargara de darle debaja</font>
                    </p>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
