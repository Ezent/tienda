@extends('plantillaInicio.welcome')
@section('carousel')
@endsection

@section('content')
<div class="container" style="margin-top:5%;">
    <div class=" row ">
        <div class="col-md-2 "> </div>
        <div class="col-md-8 ">
            <div class="card ">
                <div class="card-header bg-danger ">
                    <h6>Este codigo de confirmación ya ha caducado</h6>
                </div>
                <div class="card-body ">
                    <h6>
                        <p class="alert-danger" align="justify"> Este código de confirmación ya ha sido usado o se ha vencido el tiempo de espera para la validación,
                            si desea solicitar otro nuevo código de verificación, intente <a href="/login">iniciar sesión</a>                            con su correo y contraseña, el sistema le enviara un nuevo correo electrónico, le pedimos este
                            atento y lo conteste a la brevedad posible</a>
                        </p>
                    </h6>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function validarPass(){
        let pass1=$('#pass1').val();
        let pass2=$('#pass2').val();

        if(pass1!=pass2){
            $('#msj').css({'display':'inline'});
        return false;
        }else{
            $('#msj').css({'display':'none'});
            return true;
        }
    }

</script>
@endsection

@section('footer')
<div class="fixed-bottom">
    @include('plantillaInicio.footer')
</div>
@endsection
