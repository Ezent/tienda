<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            <div class="size-box carousel-item active">
                <img class="img-fluid size-img" src="{{ asset('img/imagen1.png') }}"> {{--
                <div class="carousel-caption d-none d-md-block">
                    <h3>First Slide</h3>
                    <p>This is a description for the first slide.</p>
                </div> --}}
            </div>
            <!-- Slide Two - Set the background image for this slide in the line below -->
            <div class="size-box carousel-item">
                <img class="img-fluid size-img sombra" src="{{ asset('img/imagen2.jpg') }}">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Amacas</h3>
                    <p>Contamos con una gran variedad de amacas</p>
                </div>
            </div>
            <!-- Slide Three - Set the background image for this slide in the line below -->
            <div class="size-box carousel-item">
                <img class="img-fluid size-img sombra" src="{{ asset('img/imagen3.jpg') }}">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Recuerdos</h3>
                    <p>Ven a conocer Merida Yucatan y llevate un hermoso recuerdo</p>
                </div>
            </div>
        </div>

        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
    </div>
</header>
