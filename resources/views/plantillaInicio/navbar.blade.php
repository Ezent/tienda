<nav class='navbar fixed-top navbar-expand-lg navbar-light fixed-top' style="background: #3681AF;">
    <div class='container'>
        <a class='navbar-brand' href='/'>Shop Montecristo</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive'
            aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'>
            <span class='navbar-toggler-icon' />
        </button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class="navbar-nav mr-auto">
                <li class='nav-item dropdown'>
                    <a class='nav-link dropdown-toggle' href='#' id='navbarDropdownBlog' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                 Categorias
                            </a>
                    <div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdownBlog'>
                        <a class='dropdown-item' href='full-width.html'>Full Width Page</a>
                        <a class='dropdown-item' href='sidebar.html'>Sidebar Page</a>
                        <a class='dropdown-item' href='faq.html'>FAQ</a>
                        <a class='dropdown-item' href='404.html'>404</a>
                        <a class='dropdown-item' href='pricing.html'>Pricing Table</a>
                    </div>
                </li>
                <li class="nav-item">
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </li>
            </ul>
            <ul class='navbar-nav ml-auto'>

                <!--Menu dinamico -->
                @if(auth()->user())
                <!-- Menu del usuario-->
                <div class="nav-item dropdown " style="cursor: pointer;">
                    <a id="dLabel" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{--   <span class="fa fa-user"></span>  --}}
                    {{ auth()->user()->name }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dLabel" style="background: #D4E8E6;">
                        <div class="px-4 py-9">
                            <div class="container-fluid">
                                <div class="row">
                                    <p align="center">
                                        <img src="{{ asset('img/user.png') }}" class="rounded img-fluid" width="30%" height="30%">
                                    </p>
                                </div>
                            </div>

                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('mi-cuenta') }}">Mi cuenta</a>
                            <a class="dropdown-item" href="#">Notificaciones
                                <span class="badge "  style="background: #66C89F; color: white;">6<span>
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{{ route('close_session') }}}">Cerrar sesión</a>
                        </div>
                    </div>
                </div>
                @endif
                <!--Fin del menu dinamico-->
                <!-- En caso de que no exista sesion -->
                @if (!auth()->user())
                <li class='nav-item'>
                    <a class='nav-link' href='{{ route("login") }}'>Iniciar Sesion</a>
                </li>
                <li class='nav-item'>
                    <a class='nav-link' href='{{ url("register") }}'>Registrarse</a>
                </li>
                @endif

            </ul>
        </div>
    </div>
</nav>