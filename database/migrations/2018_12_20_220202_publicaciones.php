<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Publicaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicaciones', function (Blueprint $table) {
            $table->increments('id');
            //  $table->integer('proveedor_id')->unsigned();
            $table->integer('producto_id')->unsigned();
            $table->integer('users_id')->unsigned();
            $table->string('nombrePublicacion', 200);
            $table->integer('precio');
            $table->string('descripcion', 800);
            $table->string('estado', 45)->default('ALTA');
            $table->timestamp('registro')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publicaciones');
    }
}