<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Proveedor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 45);
            $table->string('apellidos', 75);
            $table->string('direccion', 335)->nullable();
            $table->string('cp', 15)->nullable();
            $table->string('estado', 60)->nullable();
            $table->string('pais', 60)->nullable();
            $table->string('telefono', 45)->nullable();
            $table->string('cel', 35)->nullable();
            $table->string('num_cuenta', 22)->nullable();
            $table->string('banco', 25)->nullable();
            $table->string('empresa', 85)->nullable();
            $table->string('url', 75)->nullable();
            $table->string('correo', 60)->unique();
            $table->string('pass', 255);
            $table->string('clave', 45);
            $table->string('status_prov', 45)->nullable()->default('en proceso');
            $table->dateTime('fecha_alta');
            $table->dateTime('fecha_baja')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proveedor');
    }
}
