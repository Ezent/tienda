<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProvSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('proveedor')->insert([
            'nombre' => 'Brenda',
            'apellidos' => 'Garcia',
            'direccion' => 'salsi puedes',
            'cp' => '1232',
            'estado' => 'Tabasco',
            'pais' => 'México',
            'telefono' => '993933000',
            'cel' => '993000000',
            'num_cuenta' => '3434123412341234',
            'banco' => 'HSBC',
            'empresa' => 'patitos',
            'url' => 'www.patitos.com',
            'correo' => 'brenda@gmail.com',
            'pass' => '1212',
            'clave' => '1212',
            'fecha_alta' => '2018-12-20 17:39:34',
        ]);

    }
}
