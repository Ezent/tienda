<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categoria')->insert([
            'categoria'=>'Autos'
        ]);
        DB::table('categoria')->insert([
            'categoria'=>'Moda'
        ]);
        DB::table('categoria')->insert([
            'categoria'=>'Electronica'
        ]);
        DB::table('categoria')->insert([
            'categoria'=>'Salud'
        ]);
        DB::table('categoria')->insert([
            'categoria'=>'Deportes'
        ]);
        DB::table('categoria')->insert([
            'categoria'=>'Cocina'
        ]);
        DB::table('categoria')->insert([
            'categoria'=>'Linea Blanca'
        ]);
    }
}
