<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**Aqui se agregan todos nuestros seeder para rellenar las bd**/
       $this->call(ProvSeeder::class);
        $this->call(CategoriaSeeder::class);
    }
}
