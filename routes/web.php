<?php

/* require __DIR__ . '\proveedores\rutas_proveedores.php'; */

/*Route start */
Route::get('/', 'Inicio\InicioController@show_view')->name('start');

Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('home');
/*ruta de validacion */
Route::get('register/verify/{code}', 'Auth\VerifyModifiedController@verify');
Route::get('/verify/confirmation', 'Auth\VerifyModifiedController@showAlert')->name('lock');
Route::get('/link/expired', 'Auth\VerifyModifiedController@expiredLink')->name('expired-link-verificacion');
Route::get('/resend/email', 'Auth\VerifyModifiedController@resendEmail')->name('resendEmail-verificacion');
/*Ruta de prueba admin */
Route::get('/prov', function () {
    return view('admin.template.template_1');
})->name('admin');

/*ruta para recuperación de contraseña */
Route::get('/recover/password', 'Auth\VerifyModifiedController@showRecoverPass')->name('show-recoverPass');
Route::post('/recover/password', 'Auth\VerifyModifiedController@recoverPass')->name('recover-password');
/*ruta para cerrar sesiones */
Route::get('/session/close', 'Auth\VerifyModifiedController@logout')->name('close_session');
/*Routes for menu */
include 'menu/route_menu.php';