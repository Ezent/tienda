<?php
/*Mi cuenta */
//Route::get('/mi-cuenta', '')->name('mi_cuenta');
/*ruta  de prueba plantilla de menu */
Route::get('/mi-cuenta/menu', function () {
    return view('template_menu.template');
})->name('mi-cuenta')->middleware('verified');

/*Categorias */
Route::get('/menu/categorias','Categorias\CategoriasController@index')->name('view-catorias');
Route::any('/menu/categorias/del','Categorias\CategoriasController@deleteCategoria')->name('delete-categorias');
/*Fin categorias*/

/*Ventas */
Route::any('/mi-cuenta/ventas/publicaciones', 'ventas\PublicacionesController@show')->name('ventas_publicaciones'); /*get y post para los ajax de ver, baja y eliminar, mas estetico, mas dinamico, menos carga del server */
Route::any('/mi-cuenta/ventas/publicaciones/form/{id?}', 'ventas\PublicacionesController@show_form')->name('ventas_publicaciones_form');
Route::post('/mi-cuenta/ventas/publicaciones/form_add', 'ventas\PublicacionesController@add_publicaciones')->name('ventas_publicaciones_form_add');
Route::any('/mi-cuenta/ventas/publicaciones/form_edit/{id?}', 'ventas\PublicacionesController@editPublication_show')->name('ventas_publicaciones_edit');
Route::post('/mi-cuenta/ventas/publicaciones/form_low', 'ventas\PublicacionesController@low_up_Publication')->name('ventas_publicaciones_low_up');
Route::post('/mi-cuenta/ventas/publicaciones/edit', 'ventas\PublicacionesController@editPublication')->name('ventas_publicacionesEdit');


/*ajax publicaciones */
Route::post('/mi-cuenta/ventas/publicaciones/delete_foto/{id?}', 'ventas\PublicacionesController@deleteFoto')->name('ventas_publicaciones_delF');
Route::post('/mi-cuenta/ventas/publicaciones/delete_pub/{id?}', 'ventas\PublicacionesController@deletePublication')->name('ventas_publicaciones_delP');
