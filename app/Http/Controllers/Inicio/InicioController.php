<?php
namespace App\Http\Controllers\Inicio;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class InicioController extends Controller
{
    protected $request;

    public function show_view(Request $request)
    {
        if ($request->user()) {
            $validate = User::where('id', $request->user()->id)->first();
            if ($validate->email_verified_at == null) {
                return redirect()->route('lock');
            }
        }

        return view('plantillaInicio.welcome');
    }

    public function validar(Request $request)
    {
        return view('plantillaInicio.welcome');
    }

    /*funcion para ejecutar como middleware */
    public function validation_email(Request $request)
    {
        if ($request->user()) {
            $validate = User::where('id', $request->user()->id)->first();
            if ($validate->email_verified_at == null) {
                return redirect()->route('lock');
            }
        }
    }

}
