<?php

namespace App\Http\Controllers\Categorias;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class CategoriasController extends Controller
{

    function __construct()
    {
        $this->middleware('verified',[
            'only'=>['index']
        ]);
    }

function index(){
    $categoria=DB::table('categoria')->get();
    $subcategoria=DB::table('subcategoria')->get();
    $producto=DB::table('producto')->get();
    return view('menu-cuenta.Categorias.categoria',
    ['categoria'=>$categoria,
    'subcategoria'=>$subcategoria,
    'producto'=>$producto
    ]);
}

/*funcion para eliminar una categoria */
public function deleteCategoria(Request $request){
    return "no ajax";
        if($request->ajax()){
            return "Algo";
           //return response()->json(["msj"=>"algo"]);
            /*$delete=DB::table('categoria')>where('id','=',$request->id_cat);
            if($delete){
                return "Eliminado Correctamente";
            }*/
        }
}

}
