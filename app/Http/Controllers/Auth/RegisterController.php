<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
/*mail */
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    /*vamos a personalizar nuestros mensajes */
    public function msj()
    {
        return array(
            'name.max' => 'Excediste el numero de carácteres',
            'last_name.max' => 'Excediste el numero de carácteres',
            'email.unique' => 'Este correo ya esta en uso, intenta con otro!!!',
            'email.email' => 'Tu correo electrónico no es válido',
            'password.min' => 'El minimo de carácteres son 6, recuerda mezclar, mayúsculas, minúsculas, numeros y carácteres',
        );
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max: 100'],
            'last_name' => ['required', 'string', 'max: 100'],
            'email' => ['required', 'string', 'email', 'max:100', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ], $this->msj());
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $data['pass_encrypted'] = Hash::make($data['password']);
        $data['confirmation_code'] = rand(1, 7000000);

        $user = User::create([
            'name' => $data['name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => $data['pass_encrypted'],
            'clave' => $data['password'],
            /*Aqui va el campo de confirmacion */
            'confirmado' => $data['confirmation_code'],
        ]);

        /*   return User::create([
        'name' => $data['name'],
        'last_name' => $data['last_name'],
        'email' => $data['email'],
        'password' => Hash::make($data['password']),
        'clave' => $data['password'],
        ]); */

        Mail::send('verification_email.send-email', $data, function ($message) use ($data) {
            $message->from('zch.radox13@gmail.com', 'Shop Montecristo');
            $message->to($data['email']);
            $message->subject('Por favor confirma tu correo electrónico ;)');
        });

        return $user;
    }
}
