<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class VerifyModifiedController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth', ['only' => ['showAlert', 'resendEmail', 'expiredLink']]);
    }

    public function showAlert(Request $request)
    {
        if (!$request->user()->email_verified_at == null) {
            return redirect()->route('start');
        }
        return view('verification_email/lock-confirmation');
    }

    /*una funcion para la validacion del correo para la validacion por correo */
    public function verify($code)
    {
        $validation = User::where('confirmado', '=', $code)->first();
        if (!$validation) {
            return redirect()->route('expired-link-verificacion');
        } else {
            $validation->confirmado = "done";
            $validation->email_verified_at = now();
            $validation->save();
            return redirect()->route('start');
        }
    }
    /*Funcion para reenviar el correo */
    public function resendEmail(Request $request)
    {
        $data['confirmation_code'] = $request->user()->confirmado;
        $data['email'] = $request->user()->email;
        Mail::send('verification_email.send-email', $data, function ($message) use ($data) {
            $message->from('ezent@gmx.es', 'Shop Montecristo');
            $message->to($data['email']);
            $message->subject('Por favor confirma tu correo electrónico ;)');
        });
        return back()->with('msj', 'Correo de confirmación enviado nuevamente, revisa tu bandeja de entrada');
    }

    public function expiredLink()
    {
        return view('verification_email.link_expired');
    }
    /*funcion para restablecer contraseña por correo */
    public function showRecoverPass()
    {
        return view('auth.recover-pass');
    }

    public function recoverPass(Request $request)
    {
        $msj = array(
            "email.email" => "Escribe un correo con la estructura válida",
        );
        $user = User::where('email', '=', $request->email)->first();
        if ($user == null) {
            return back()->with(
                ['msj' => 'Este correo no corresponde a ningun usuario registrado en nuestra base de datos',
                    'msj2' => 'danger']);
        } else {
            /*send email for recover password */
            $data_recover['email'] = $request->email;
            $data_recover['clave'] = $user->clave;
            Mail::send('verification_email.recover-pass-sendMail', $data_recover, function ($message) use ($data_recover) {
                $message->from('ezent@gmx.es', 'Shop Montecristo');
                $message->to($data_recover['email']);
                $message->subject('Recuperación de Contraseña;)');
            });
            return back()->with([
                'msj' => 'El correo de recuperación se ha enviado correctamente, verififca tu bandeja de entrada.',
                'msj2' => 'success',
            ]);
        }
    }

    /*Logout */
    public function logout()
    {
        Auth::logout();
        return redirect()->route('start');
    }
}
