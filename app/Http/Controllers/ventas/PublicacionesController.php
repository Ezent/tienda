<?php

namespace App\Http\Controllers\ventas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
/*Para conectar al bucket de aws */
use Illuminate\Support\Facades\Storage;

class PublicacionesController extends Controller
{
    /*constructor */
    public function __construct()
    {
        $this->middleware('verified',
         ['only' =>
         ['show', 'show_form','editPublication_show','add_publicaciones']]
        );
    }
    /*Vistas*/
    public function show(Request $request)
    {
        if($request->ajax()){
            $ver=DB::table('publicaciones as p')
            ->join('users as u','u.id','=','p.users_id')
            ->join('fotos_pub as fp','fp.publicaciones_id','=','p.id')
            ->join('producto as pr','pr.id','=','p.producto_id')
            ->where('u.id','=',auth()->user()->id)
            ->where('p.id','=',$request->id)
            ->select('p.id as id','foto','principal','nombrePublicacion as publicacion','precio','descripcion','producto','estado','registro as fecha')
            ->get();
            return response()->json($ver);
        }
      /* $data=DB::table('publicaciones as p')
      ->join('users as u','u.id','=','p.users_id')
      ->leftJoin('fotos_pub as fp','fp.publicaciones_id','=','p.id')
      ->join('producto as pr','pr.id','=','p.producto_id')
      ->where('fp.principal','=','yes')
      ->where('u.id','=',auth()->user()->id)
      ->select('p.id as id','foto','nombrePublicacion as publicacion','precio','descripcion','producto','estado','registro as fecha')
      ->get(); */
      $data=DB::select('select p.id as id, foto,nombrePublicacion as publicacion,precio,descripcion,producto,estado,registro as fecha
      from publicaciones as p
      join users as u on u.id=p.users_id
      left join fotos_pub as fp on fp.publicaciones_id=p.id and fp.principal="yes"
      join producto as pr on pr.id=p.producto_id
      and u.id='.auth()->user()->id.';');
     // dd($data);
        return view('menu-cuenta.ventas.publicaciones',compact('data'));
    }

    public function show_form(Request $request, $id = 0)
    {
        /*Aplicar ajax para carga de datos */
        if ($request->ajax()) {
            if ($id == 1) {
                $subcategoria = DB::table('subcategoria as s')->where('s.categoria_id', '=', $request->id)->get();
                return response()->json($subcategoria);
            } else if ($id == 2) {
                $producto = DB::table('producto as p')->where('p.subcategoria_id', '=', $request->id)->get();
                return response()->json($producto);
            }
        }
        $categoria = DB::table('categoria')->get();
        return view('menu-cuenta.ventas.publicaciones_form', ['categoria' => $categoria]);
    }
    /*agregando las publicaciones */
    public function add_publicaciones(Request $request)
    {
        $this->validate($request, [
            'img_principal' => 'mimes:jpeg,png,gif,jpg',
            'img_desc.*' => 'mimes:jpeg,png,gif,jpg',
        ],
            [
                'img_principal.mimes' => 'La imagen principal no es un archivo de tipo [imagen]',
                'img_desc.*.mimes' => 'Las imagenes descriptivas no son de tipo [imagen]',
            ]);
        /*Insertando valores en la tabla publicaciones */
         DB::table('publicaciones')->insert([
        'nombrePublicacion' => $request->nombrePublicacion,
        'precio' => $request->precio,
        'descripcion' => $request->descripcion,
        'producto_id' => $request->producto,
        'users_id' => request()->user()->id,
        ]);
        /*Obtener el ultimo registro insertado con un stored procedure */
        $id=DB::select('call publicacion_id()');
        /* Insertando valores en la tabla fotos_pub*/
        $flag = count($request->img_desc) + 1;
        for ($i = 0; $i < $flag; $i++) {
                if($i>=count($request->img_desc)){
                    $name = 'p_'.rand() . '_' . $request->file('img_principal')->getClientOriginalName();
                    $this->cargarImgBucket($request->file('img_principal') , $name);
                }else{
                    $data=$request->img_desc[$i];
                    $name = rand() . '_' . $data->getClientOriginalName();
                   $this->cargarImgBucket($data,$name);
                }
                DB::table('fotos_pub')->insert([
                    'foto' => $name,
                    'principal' => ($i >= count($request->img_desc)) ? 'yes' : 'no',
                    'publicaciones_id' => $id[0]->id, //id de la publicaciones para vincular las fotos
                    ]);
            }

        return redirect()->route('ventas_publicaciones_form')->with('msj', 'Publicación añadida con éxito');
    }

    /*baja */
    function low_up_Publication(Request $request){
        if($request->ajax()){
            if($request->action=='baja'){
            $low=DB::table('publicaciones')->where('id','=',$request->id)->update([
                'estado'=>'BAJA'
            ]);
            return "La publicación ha sido dada de baja ";
        }
        else if($request->action=='alta'){
            $low=DB::table('publicaciones')->where('id','=',$request->id)->update([
                'estado'=>'ALTA'
            ]);
            return "La publicación se ha activado";
        }
        }
    }
    /*Editar publicacion */
    function editPublication_show(Request $request, $id=0){
             /*Aplicar ajax para carga de datos */
             if ($request->ajax()) {
                if ($id == 1) {
                    $subcategoria = DB::table('subcategoria as s')->where('s.categoria_id', '=', $request->id)->get();
                    return response()->json($subcategoria);
                } else if ($id == 2) {
                    $producto = DB::table('producto as p')->where('p.subcategoria_id', '=', $request->id)->get();
                    return response()->json($producto);
                }
            }
            $edit=DB::table('publicaciones as p')->join('producto as pr','pr.id','=','p.producto_id')
            ->join('subcategoria as sub','sub.id','=','pr.subcategoria_id')
            ->join('categoria as c','c.id','=','sub.categoria_id')
            ->where('p.id','=',$id)
            ->select('p.id','c.categoria','c.id as categoria_id','p.nombrePublicacion','p.precio','p.descripcion',
            'pr.producto','pr.id as producto_id','sub.id as subcategoria_id')->first();
            $categoria = DB::table('categoria')->get();
            $subcategoria=DB::table('subcategoria')->get();
            $producto=DB::table('producto')->get();
            /*Encontrad errores y os corrijais */
            $foto=DB::table('publicaciones as p')->join('users as u','p.users_id','=','u.id')
            ->join('fotos_pub as fp','fp.publicaciones_id','=','p.id')->where('u.id','=',auth()->user()->id)
            ->where('p.id','=',$id)->orderBy('fp.principal','desc')->select('fp.id','fp.foto','fp.principal')->get();
        return view('menu-cuenta.ventas.publicaciones_form_edit',
        ['categoria'=>$categoria,'edit'=>$edit,'subcategoria'=>$subcategoria,'producto'=>$producto,'foto'=>$foto]);
    }
    /*funcion para editar publicación */
    function editPublication(Request $request){
        /*Modificar la publicacion de la base de datos */
        DB::table('publicaciones')->where('id','=',$request->id_publicaciones)->update(
            [
                'nombrePublicacion'=>$request->nombrePublicacion,
                'precio'=>$request->precio,
                'producto_id'=>$request->producto,
                'descripcion'=>$request->descripcion
            ]);

      /*Editar*/
      if($request->file('img_p')){
        $data=$request->img_p;
        $name = rand() . '_' . $data->getClientOriginalName();
       $this->cargarImgBucket($data,$name);
          DB::table('fotos_pub')->where('id','=',$request->id_foto_update[0])->update([
              'foto'=>$name
          ]);
          $this->beforeDeleteUpdate($request->name_foto_p);
      }
      if($request->file('img_desc')){
          $indices=array_keys($request->img_desc);
          for ($i=0; $i < count($indices) ; $i++) {
              if(file($request->img_desc[$indices[$i]])){
                  $data=$request->img_desc[$indices[$i]];
                  $name = rand() . '_' . $data->getClientOriginalName();
                  $this->cargarImgBucket($data,$name);
                  DB::table('fotos_pub')->where('id','=',$indices[$i])->update([
                    'foto'=>$name
                ]);
                //delete before img
                $this->beforeDeleteUpdate($request->name_foto_desc[$indices[$i]]);
                }
          }
    }
    /**Validacion los inputs de tipo file */
        if($request->file('img_desc_new')){
            for ($i=0; $i < count($request->img_desc_new) ; $i++) {
               if(file($request->img_desc_new[$i])){
                   $data=$request->img_desc_new[$i];
                   $name=rand().'_'.$data->getClientOriginalName();
                   $this->cargarImgBucket($data,$name);
                   DB::table('fotos_pub')->insert([
                    'publicaciones_id'=>$request->id_publicaciones,
                    'foto'=>$name,
                    'principal'=>'no'
                ]);
               }
            }
        }
        return redirect('/mi-cuenta/ventas/publicaciones/form_edit/'.$request->id_publicaciones)->with('msj','Actualizado correctamente!!!');
    }
    /*funcion para eliminar las fotos de la publicación */
    function beforeDeleteUpdate($img){
        $result=Storage::disk('s3')->delete($img);
        if($result){
            return true;
        }else{
            return false;
        }
    }
    public function cargarImgBucket($data,$name)
    {
      //  $nombre = rand() . '_' . $data->getClientOriginalName();
        Storage::disk('s3')->put($name, fopen($data, 'r+'), 'public');
    }

    /*funciones ajax */
    /*Delete publication */
    function deletePublication(Request $request){
       if($request->ajax()){
          $data=DB::table('fotos_pub')->where('publicaciones_id','=',$request->id)->select('foto')->get();
          if(!$data->isEmpty()){
              DB::table('publicaciones')->where('id','=',$request->id)->delete();
              for ($i=0; $i < count($data); $i++) {
                  $this->beforeDeleteUpdate($data[$i]->foto);
              }
              return "Eliminada";
          }else{
              return 'vacío';
          }
       }
    }
    /*Delete picture */
    public function deleteFoto(Request $request, $id=0){
        //notese un error que por la version de laravel, la 5.7 no es muy exigente con el ajax de igual forma lo ejecuta, quizas porque mi ajax es de tipo post y no el clásico ajax de jquery que lleva la palabra reservada ajax
        //por tal motivo ya no se solicito que se le validara si la peticion es de tipo ajax algo que era  obligatoruio realizar en la version de laravel 5.6
        $result=Storage::disk('s3')->delete($request->img);
        $delete=DB::table('fotos_pub')->where('id','=',$request->id)->delete();
        if($delete){
            return "Eliminado correctamente";
           }
    /*     if($result){
        $delete=DB::table('fotos_pub')->where('id','=',$request->id)->delete();
        return "Eliminado correctamente";
       } */
        return 'No se ha podido procesar la petición';
    }
}
